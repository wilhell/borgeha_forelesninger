import random
'''

Funksjonen beskriv_set tar inn to LISTER a og b
Den skal gjøre om listene til set, og så
skrive ut ulike ting:
- alle unike verdier som er i begge listene til sammen
- alle verdier som KUN er i begge listene
- alle verdier som er i begge listene, men ikke i begge
- elementene som er i a, men ikke i b

'''

def beskriv_sett(a, b):
    
    # Gjør om begge til set
    # ...

# fiks disse:
#    print(f'Unike element i hele a og b tilsammen: {}')
#    print(f'Unike element som er i både a og b: {}')
#    print(f'Unike element i som ikke er i begge lister: {}')
#    print(f'Elementer i a som ikke også er i b: {}')


a = [random.randrange(10) for i in range (0, 10)]
b = [random.randrange(10) for i in range (5, 15)]
