import random

'''
Opprette tom dictionary/ordbok
Gjøre om strengen til liste av ord
Gå igjennom hvert ord og:
 - Er ordet i dict fra før? Legg på 1 på verdien som er der
 - Hva skal du gjøre hvis ordet ikke er det fra før? Se på d.get igjen.
Returner dictionary
'''

def count_words(streng):
    # Gjør det over
    return {} # Erstatt med din utfylte dict
    
import random
# Jeg lager bare kjapt en streng med en drøss tilfeldige tall mellom 0 og 10:
streng = " ".join([str(random.randint(0, 10)) for i in range(100)])
d = count_words(streng)

# skriver dem ut:
for key in sorted(d):
    print(f'{key} var der {d[key]} ganger.')

# Hvis dere har veldig lyst:
#import lyric
#print(count_words(lyric.TEKST))
